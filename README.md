# Course work №7 - "Tracker of userful habits (Docker version)".
# The project using Django DRF framework and Docker.

## Description
The "REST API service" of the userful habits tracker SPA application is implemented.
Allows users to register on the service and post new useful habits,
with a reminder of their fulfillment via a telegram bot.

## Set Up your settings
Create a .env configuration file with your personal settings in the root of the project,
according to the sample, specified in .env.sample.
Fill out the file according to your personal data.
- Create a telegram bot for send notifications to users and write your token into the .env file

## Project settings on your server
- Create a database in postgresql.
  (the name of the database must match the name specified in the .env file)
- Create the superuser:
    * python3 manage.py csu (run this command inside the working container)
  
--------------------------------------------------------------------

# Run the project through Docker:
Build an image and run the container with the command: docker-compose up --build

## The main working links
- To read an API documentation follow the next link: http://51.250.92.223/habits_tracker/redoc/
- To log in to the admin panel, follow this link: http://51.250.92.223/habits_tracker/admin/
- To create a new habit: http://51.250.92.223/habits_tracker/habits/create/
- To show the list of the created habits: http://51.250.92.223/habits_tracker/habits/list/